# solarus-devops

This repository includes resources for building, testing and packaging
Solarus components, as well as for generating documentation and packaging
Solarus Quests.

The main use-case is the [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) framework.

## Docker Images

To build the Docker images, use [Docker Build Bake](https://docs.docker.com/build/bake/):
```
TAG=YYYYMMDD docker buildx bake IMAGE_NAME...
```

Available image names can be found in the [Bake configuration file](docker-bake.hcl).

> **Note:** Ready-to-use images are available in the
> [Container Registry](https://gitlab.com/solarus-games/solarus-devops/container_registry)
> instance of the Solarus DevOps project.

## CI/CD for Quests

Quest makers can have their quests in GitLab automatically tested and packaged
into `.solarus` files simply by using the following `.gitlab-ci.yml` file:
```yml
include:
  - https://gitlab.com/solarus-games/solarus-devops/raw/master/gitlab/quests.yml
```

The quests pipeline can be customised by using
[GitLab CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/#via-the-ui):

* `SKIP_TESTING` set to any value will skip test jobs.
* `SKIP_PACKAGING` set to any value will skip package jobs.
* `SOLARUS_REF` (defaults to `dev`) is the Git ref to use for downloading
  the Solarus Engine artifact containing the testing tools.

For testing, quest makers must define test Lua maps in `data/maps/tests` (or a
subdirectory) that will be run automatically by the `quest-test` job in the
pipeline. Only map files beginning with the prefix `test_` will be run. Each
test map script should call `sol.main.exit()` once the test is complete.

The quests CI/CD pipeline will build the following quest packages:

* For non-releases (commits): `<project-name>-<commit-hash>.solarus`
* For releases (tags): `<project-name>-<tag-name>.solarus`

Alternatively, quest makers can also copy the complete `quests.yml`
file directly into their repositories and name it `.gitlab-ci.yml`.

More details can be found in the
[pipeline configuration file](/gitlab/quests.yml) itself.

## Pipeline Badges

Pipeline status and test coverage badges can be displayed using the
following URL templates:
```
https://gitlab.com/<namespace>/<project>/badges/<branch>/pipeline.svg
https://gitlab.com/<namespace>/<project>/badges/<branch>/coverage.svg
```

Where `<namespace>/<project>` is a project with GitLab CI/CD pipelines enabled and
`<branch>` is the desired branch to display the badge for.

Project badges can also be added in **Settings -> General -> Badges**.
