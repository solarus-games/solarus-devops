variable "TAG" {
  description = "Tag to use when building the images"
  default = "latest"
}

variable "IMAGE_VERSION" {
  description = "Version to set in the image labels"
  default = TAG
}

target "_base-build-env" {
  context = "base-build-env"
}

target "linux-build-env" {
  description = "Solarus Linux build environment image"
  context = "linux-build-env"
  contexts = {
    "base-build-env" = "target:_base-build-env"
  }
  args = {
    IMAGE_VERSION = IMAGE_VERSION
  }
  tags = ["registry.gitlab.com/solarus-games/solarus-devops/linux-build-env:${TAG}"]
}

target "mingw-build-env" {
  description = "Solarus MinGW build environment image"
  context = "mingw-build-env"
  contexts = {
    "base-build-env" = "target:_base-build-env"
  }
  args = {
    IMAGE_VERSION = IMAGE_VERSION
  }
  tags = ["registry.gitlab.com/solarus-games/solarus-devops/mingw-build-env:${TAG}"]
}

target "android-build-env" {
  description = "Solarus Android build environment image"
  context = "android-build-env"
  args = {
    IMAGE_VERSION = IMAGE_VERSION
  }
  tags = ["registry.gitlab.com/solarus-games/solarus-devops/android-build-env:${TAG}"]
}

target "docs-build-env" {
  description = "Solarus documentation build environment image"
  context = "docs-build-env"
  args = {
    IMAGE_VERSION = IMAGE_VERSION
  }
  tags = ["registry.gitlab.com/solarus-games/solarus-devops/docs-build-env:${TAG}"]
}

target "utilities-env" {
  description = "Solarus utilities environment image"
  context = "utilities-env"
  args = {
    IMAGE_VERSION = IMAGE_VERSION
  }
  tags = ["registry.gitlab.com/solarus-games/solarus-devops/utilities-env:${TAG}"]
}
