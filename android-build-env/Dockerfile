# syntax=docker/dockerfile:1
FROM debian:bookworm-slim

# Image labels
ARG IMAGE_VERSION=latest
LABEL description="Solarus Android build environment image" \
      maintainer="The Solarus Team" \
      version="${IMAGE_VERSION}" \
      org.solarus-games.pkglist-cmd="dpkg -l"

# Configure shell to use during the build
SHELL ["/bin/bash", "-c"]

# Configure image environment
ENV ANDROID_HOME=/opt/android-sdk
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH="${ANDROID_HOME}/cmdline-tools/latest/bin:${JAVA_HOME}/bin:${PATH}"

# Install required packages
RUN apt-get update \
    && apt-get install --assume-yes --no-install-recommends \
        ca-certificates \
        ccache \
        curl \
        file \
        patch \
        unzip \
    && rm -rf /var/lib/apt/lists/*

# Copy Java JDK 17 from Eclipse Temurin
COPY --from=eclipse-temurin:17-jdk ${JAVA_HOME} ${JAVA_HOME}

# Download and install the Android SDK command line tools
ARG CMDLINETOOLS_ZIP=https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip
RUN curl --proto "=https" -L ${CMDLINETOOLS_ZIP} -o commandlinetools-linux.zip \
    && unzip -d cmdlinetools-linux commandlinetools-linux.zip \
    && mkdir -p ${ANDROID_HOME}/cmdline-tools \
    && mv cmdlinetools-linux/cmdline-tools ${ANDROID_HOME}/cmdline-tools/latest \
    && rm -f commandlinetools-linux.zip \
    && rmdir cmdlinetools-linux

# Configure the Android SDK and install required packages
# Ref: https://gitlab.com/solarus-games/solarus-android/-/blob/master/gradle/libs.versions.toml
ARG COMPILE_SDK=34
ARG BUILD_TOOLS_VERSION=35.0.1
ARG NDK_VERSION=28.0.13004108
ARG CMAKE_VERSION=3.31.5
RUN set -o pipefail \
    && mkdir -p $HOME/.android && touch $HOME/.android/repositories.cfg \
    && yes | sdkmanager --licenses || { ec=$?; [[ $ec -eq 141 ]] && true || ( exit $ec ); } \
    && echo y | sdkmanager \
        "platforms;android-${COMPILE_SDK}" \
        "platform-tools" \
        "build-tools;${BUILD_TOOLS_VERSION}" \
        "ndk;${NDK_VERSION}" \
        "cmake;${CMAKE_VERSION}"
