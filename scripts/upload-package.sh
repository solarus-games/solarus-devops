#!/usr/bin/env bash
# Helper script for uploading packages to the GitLab package registry.
# script by gitlab.com/hhromic

set -Eeuo pipefail

declare -r GITLAB_API_BASE=https://gitlab.com/api/v4
declare -r PROJECTS_API=$GITLAB_API_BASE/projects

if (( $# < 4 )); then
  cat << __EOF__
Helper script for uploading packages to the GitLab package registry.
Usage: $0 GITLAB_PROJECT PACKAGE_NAME PACKAGE_VERSION PACKAGE_FILE

Positional arguments:

    GITLAB_PROJECT     ID or URL-encoded path of the GitLab project
    PACKAGE_NAME       package name for publishing to the package registry
    PACKAGE_VERSION    package version for publishing to the package registry
    PACKAGE_FILE       local package file to upload to the package registry

Environment variables:

    GITLAB_TOKEN       GitLab personal access token with at least 'api' scope

__EOF__
  exit 0
fi

declare -r GITLAB_PROJECT=$1
declare -r PACKAGE_NAME=$2
declare -r PACKAGE_VERSION=$3
declare -r PACKAGE_FILE=$4

if [[ -z ${GITLAB_TOKEN:-} ]]; then
  printf "error: GITLAB_TOKEN: missing required environment variable\n" >&2
  exit 1
fi

printf "Uploading package file '%s' for GitLab project '%s' ...\n" "$PACKAGE_FILE" "$GITLAB_PROJECT"
RESPONSE=$(curl \
  --fail \
  --header "Private-Token: $GITLAB_TOKEN" \
  --upload-file "$PACKAGE_FILE" \
  "$PROJECTS_API/$GITLAB_PROJECT/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION/${PACKAGE_FILE##*/}")
printf "Response: %s\n" "$RESPONSE"
