#!/usr/bin/env bash
# Recursively add content to an existing compressed package.
# script by gitlab.com/hhromic
#
# Usage: $0 <PACKAGE> <BASEDIR> <PATH> ...
#        PACKAGE is the name of the package, e.g. "package.zip"
#        BASEDIR is the base directory to use inside the package
#        PATH are the files or directories to add under BASEDIR

set -Eeuo pipefail

function pkg_add() {
  local tmpdir
  local pkgdir
  tmpdir=$(mktemp -d)
  pkgdir=$tmpdir/$2
  mkdir -p "$pkgdir"
  for path in "${@:3}"; do
    cp -rf "$path" "$pkgdir"/
  done
  7za a "$1" "$pkgdir"
  rm -rf "$tmpdir"
}

pkg_add "$1" "$2" "${@:3}"
