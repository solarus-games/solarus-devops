#!/usr/bin/env bash
# Package MinGW-built binaries and their dependencies for distribution.
# script by gitlab.com/hhromic
#
# Usage: $0 <ARCH> <PACKAGE> <BASEDIR> <BINARY_OBJ> ...
#        ARCH is the MinGW architecture, i.e. "i686" or "x86_64"
#        PACKAGE is the name of the output package, e.g. "package.zip"
#        BASEDIR is the base directory to use inside the package
#        BINARY_OBJ are the binary object(s) to process

set -Eeuo pipefail

function make_pkg() {
  local tmpdir
  local pkgdir
  local obj
  local -a dep
  tmpdir=$(mktemp -d)
  pkgdir=$tmpdir/$3
  mkdir -p "$pkgdir"
  for obj in "${@:4}"; do
    cp -f "$obj" "$pkgdir"/
    while IFS=$'\t' read -r -a dep; do
      cp -f "${dep[0]}" "$pkgdir"/"${dep[1]}"
    done < <(mingw_find_deps.sh "$1" "$obj")
  done
  7za a "$2" "$pkgdir"
  rm -rf "$tmpdir"
}

make_pkg "$1" "$2" "$3" "${@:4}"
