# Changelog

This file documents important changes in the Solarus DevOps infrastructure.

All Docker images are available in the
[Container Registry](https://gitlab.com/solarus-games/solarus-devops/container_registry)
instance of the Solarus DevOps project.

Unless otherwise stated, all Docker images are tagged using the same version for a release.

## Version `20250217`

### General Notes

* This is the second stable version of the Docker build images for the Solarus CI/CD framework.
* These images are used to build Solarus components in the `v2.0.x` version range.
* The AUR helper for the Arch Linux images was switched from `yay` to `paru`.
* The SDL2 library for Linux in the Arch Linux image is installed from the AUR.
  * The official `sdl2` package was replaced by `sdl2-compat` and `sdl3`.
  * The replacement compat library is still not working properly.
* The `make` build tool in the Arch Linux images was replaced with `ninja`.
* The Qt5 dependency was replaced with Qt6.

### Software Versions

|               | linux-build-env               | mingw-build-env    | android-build-env   | docs-build-env      |
| :------------ | :---------------------------- | :----------------- | :------------------ | :------------------ |
| cmake         | `3.31.5-1`                    | `3.31.5-1`         | `3.31.5`            |                     |
| gcc           | `14.2.1+r753+g1cd744a6828f-1` | `14.2.0-1`         |                     |                     |
| glm           | `1.0.1-1`                     | `1.0.1-1`          |                     |                     |
| libmodplug    | `0.8.9.0-6`                   | `0.8.9.0-1.2`      |                     |                     |
| libvorbis     | `1.3.7-4`                     | `1.3.7-2.1`        |                     |                     |
| lua51         | `5.1.5-12`                    | `5.1.5-2.2`        |                     |                     |
| luajit        | `2.1.1736781742-1`            | `2.1.1736781742-1` |                     |                     |
| openal        | `1.24.2-1`                    | `1.24.2-1`         |                     |                     |
| physfs        | `3.2.0-2`                     | `3.2.0-1.1`        |                     |                     |
| qt6-base      | `6.8.2-2`                     | `6.8.2-1`          |                     |                     |
| qt6-svg       | `6.8.2-1`                     | `6.8.2-1`          |                     |                     |
| qt6-tools     | `6.8.2-2`                     | `6.8.2-1`          |                     |                     |
| sdl2          | `2.32.0-1`                    | `2.32.0-1`         |                     |                     |
| sdl2_image    | `2.8.5-1`                     | `2.8.5-1`          |                     |                     |
| sdl2_ttf      | `2.24.0-1`                    | `2.24.0-1`         |                     |                     |
| cmdline-tools |                               |                    | `11076708`          |                     |
| build-tools   |                               |                    | `35.0.1`            |                     |
| ndk           |                               |                    | `28.0.13004108`     |                     |
| platform      |                               |                    | `android-34`        |                     |
| doxygen       |                               |                    |                     | `1.12.0-r0`         |
| texlive       |                               |                    |                     | `20240210.69778-r8` |

## Version `20241005`

### General Notes

* This is the first stable version of the Docker build images for the Solarus CI/CD framework.
* These images are used to build Solarus components in the `v1.6.x` version range.
* The `android-build-env` image was tagged as `20241029` in this version.

### Software Versions

|               | linux-build-env               | mingw-build-env      | android-build-env   | docs-build-env      |
| :------------ | :---------------------------- | :------------------- | :------------------ | :------------------ |
| cmake         | `3.30.4-1`                    | `3.30.4-1`           | `3.30.5`            |                     |
| gcc           | `14.2.1+r134+gab884fffe3fc-1` | `14.2.0-1`           |                     |                     |
| glm           | `1.0.1-1`                     | `1.0.1-1`            |                     |                     |
| libmodplug    | `0.8.9.0-6`                   | `0.8.9.0-1.2`        |                     |                     |
| libvorbis     | `1.3.7-3`                     | `1.3.7-2.1`          |                     |                     |
| lua51         | `5.1.5-12`                    | `5.1.5-2.2`          |                     |                     |
| luajit        | `2.1.1725453128-1`            | `2.1.1725453128-1`   |                     |                     |
| openal        | `1.23.1-2`                    | `1.23.1-1.1`         |                     |                     |
| physfs        | `3.2.0-2`                     | `3.2.0-1.1`          |                     |                     |
| qt5-base      | `5.15.15+kde+r127-1`          | `5.15.15+kde+r127-1` |                     |                     |
| qt5-svg       | `5.15.15+kde+r5-1`            | `5.15.15+kde+r5-1`   |                     |                     |
| qt5-tools     | `5.15.15+kde+r3-2`            | `5.15.15+kde+r3-1`   |                     |                     |
| sdl2          | `2.30.8-1`                    | `2.30.8-1`           |                     |                     |
| sdl2_image    | `2.8.2-6`                     | `2.6.3-1.1`          |                     |                     |
| sdl2_ttf      | `2.22.0-1`                    | `2.22.0-1.1`         |                     |                     |
| cmdline-tools |                               |                      | `11076708`          |                     |
| build-tools   |                               |                      | `35.0.0`            |                     |
| ndk           |                               |                      | `27.2.12479018`     |                     |
| platform      |                               |                      | `android-34`        |                     |
| doxygen       |                               |                      |                     | `1.11.0-r0`         |
| texlive       |                               |                      |                     | `20240210.69778-r4` |
